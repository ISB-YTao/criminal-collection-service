﻿using Autofac;
using CriminalCollectionService.Core.Interfaces;
using CriminalCollectionService.Core.Services;
using Microsoft.Extensions.Configuration;

namespace CriminalCollectionService.Web
{
    public class AutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<CollectionService>().As<ICollectionService>();
            builder.RegisterType<OdysseyService>().As<IOdysseyService>();
        }
    }
}
