﻿using CriminalCollectionService.Core.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace CriminalCollectionService.Web.Controllers
{
    [Route("api/[controller]")]
    public class CollectionJobsController : Controller
    {
        private readonly ICollectionService _collectionService;

        public CollectionJobsController(ICollectionService collectionService)
        {
            _collectionService = collectionService;
        }

        [HttpGet]
        [Route("casedetails/{caseNumber}")]
        public async Task<IActionResult> GetCaseDetailByCaseNumber(string caseNumber)
        {
            await _collectionService.GetCaseDetailsByCaseNumber(caseNumber);
            return Ok();
        }

        [HttpGet]
        [Route("chargedispositions/{caseId}")]
        public async Task<IActionResult> AddDispositionForCharges(int caseId)
        {
            await _collectionService.AddDispositionForCharges(caseId);
            return Ok();
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
