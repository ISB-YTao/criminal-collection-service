using System;
using System.Xml.Serialization;

namespace CriminalCollectionService.Core.Messages
{
    [XmlRoot(ElementName = "Message")]
    public abstract class MessageBase
    {
        [XmlAttribute(AttributeName = "MessageType")]
        public string MessageType { get; set; }

        [XmlAttribute(AttributeName = "NodeID")]
        public int NodeId { get; set; }

        [XmlAttribute(AttributeName = "UserID")]
        public int UserId { get; set; }

        [XmlAttribute(AttributeName = "ReferenceNumber")]
        public string ReferenceNumber { get; set; }

        [XmlAttribute(AttributeName = "Source")]
        public string Source { get; set; }

        public string GetDefaultEfileDate()
        {
            return DateTime.Now.ToShortDateString();
        }

        public string ToSoapXml(string msg, string siteKey)
        {
            return String.Format("<soap:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'><soap:Body><OdysseyMsgExecution xmlns='http://www.tylertech.com/'><odysseyMessageXML><![CDATA[{0}]]></odysseyMessageXML><siteKey>{1}</siteKey></OdysseyMsgExecution></soap:Body></soap:Envelope>", msg, siteKey);
        }

        public string ToSoapTxnXml(string msg, string siteKey)
        {
            return String.Format("<soap:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'><soap:Body><OdysseyTxnExecution xmlns='http://www.tylertech.com/'><odysseyTransactionXML><![CDATA[{0}]]></odysseyTransactionXML><siteKey>{1}</siteKey></OdysseyTxnExecution></soap:Body></soap:Envelope>", msg, siteKey);
        }
    }
}