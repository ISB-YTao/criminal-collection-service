using CriminalCollectionService.Core.Constants;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace CriminalCollectionService.Core.Messages
{
    [XmlRoot(ElementName = "Message")]
    public class AddDispositionEvent : MessageBase
    {
        public string CaseId { get; set; }
        public string Date { get; set; }
        public string Comment { get; set; }
        public Dictionary<string, string> Charges = new Dictionary<string, string>();

        public AddDispositionEvent(){}

        public AddDispositionEvent(int nodeId, string reference, int userid, string source, string caseId, string date, string comment, Dictionary<string, string> charges)
        {
            base.NodeId = nodeId;
            base.ReferenceNumber = reference;
            base.UserId = userid;
            base.Source = source;
            this.CaseId = caseId;
            this.Date = date;
            this.Comment = comment;
            this.Charges = charges;
        }

        public string ToXml()
        {
            if (CaseId == null || Date == null) throw new Exception(CoreConstants.Helpers.MissingValuesFor("AddDispositionEvent"));         

            var msg = new StringBuilder();
            msg.Append($"<Message MessageType='AddDispositionEvent' NodeID='{this.NodeId}' ReferenceNumber='{this.ReferenceNumber}' UserID='{this.UserId}' Source='{this.Source}'>");
            msg.Append($"<CaseID>{this.CaseId}</CaseID>");
            msg.Append($"<Date>{this.Date}</Date>");
            msg.Append($"<Comment>{this.Comment}</Comment>");
            msg.Append($"<Charges>");
            foreach (var charge in Charges)
            {
                msg.Append($"<Charge><ChargeID>{charge.Key.ToString()}</ChargeID><DispositionType>{charge.Value.ToString()}</DispositionType></Charge>");
            }
            msg.Append($"</Charges></Message>");
            return base.ToSoapXml(msg.ToString(), "CASANTACLARADEVL");
        }
    }
}