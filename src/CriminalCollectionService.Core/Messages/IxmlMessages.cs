﻿namespace CriminalCollectionService.Core.Messages
{
    public static class IxmlMessages
    {

        private static string BaseSoapEnvelope =
            @"<soap:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'>
                      <soap:Body>
                        {0}
                      </soap:Body>
                    </soap:Envelope>";

        private static string BuildBaseSoapEnvelope(string body)
        {
            return string.Format(BaseSoapEnvelope, body);
        }

        private static string OdysseyMsgExecution = @"<OdysseyMsgExecution xmlns='http://www.tylertech.com/'>
                                <odysseyMessageXML>
                                    {0}
                                </odysseyMessageXML>
                                <siteKey>{1}</siteKey>
                                </OdysseyMsgExecution>";

        public static string BuildCaseByCaseNumber(string siteId, string id)
        {
            return BuildBaseSoapEnvelope(string.Format(CaseByCaseNumber, siteId, id));
        }

        private static string CaseByCaseNumber = @"
                    <LoadIntegrationXml xmlns='http://www.tylertech.com'>
                          <siteId>{0}</siteId>
                          <agency>MIB</agency>
                          <pageMode>casebycasenumber</pageMode>
                          <entityIdentifier>{1}</entityIdentifier>
                          <loadXml><![CDATA[<Case LoadMask='2147483647'><Party LoadMask='2367543'/><Fees LoadMask='990'/></Case>]]></loadXml>
                        </LoadIntegrationXml>";

        public static string BuildChargesByCaseId(string siteId, string caseId)
        {
            return BuildBaseSoapEnvelope(string.Format(ChargesByCaseId, siteId, caseId));
        }

        private static string ChargesByCaseId = @"
                    <LoadIntegrationXml xmlns='http://www.tylertech.com'>
                          <siteId>{0}</siteId>
                          <agency>MIB</agency>
                          <pageMode>casebyinternalcaseid</pageMode>
                          <entityIdentifier>{1}</entityIdentifier>
                          <loadXml><![CDATA[<Case LoadMask='20'></Case>]]></loadXml>
                        </LoadIntegrationXml>";
    }
}
