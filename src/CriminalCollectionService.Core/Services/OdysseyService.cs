﻿using CriminalCollectionService.Core.Constants;
using CriminalCollectionService.Core.Exceptions;
using CriminalCollectionService.Core.Messages;
using CriminalCollectionService.Core.Models;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using CriminalCollectionService.Core.Interfaces;
using CriminalCollectionService.Core.VOs;
using Microsoft.Extensions.Options;

namespace CriminalCollectionService.Core.Services
{
    public class OdysseyService : IOdysseyService
    {
        private readonly RestClient _restClientIxml;
        private readonly RestClient _restClientApi;
        private readonly string _odysseySiteId;
        private readonly OdysseySettingsVo _odysseySettings;

        public OdysseyService(IOptions<OdysseySettingsVo> odysseySettings)
        {
            _restClientIxml = new RestClient(new Uri(odysseySettings.Value.OdysseyIxmlServer + odysseySettings.Value.OdysseyIxmlEndpoint));
            _restClientApi = new RestClient(new Uri(odysseySettings.Value.OdysseyApiServer + odysseySettings.Value.OdysseyApiEndpoint));
            _odysseySiteId = odysseySettings.Value.OdysseySiteKey;
            _odysseySettings = odysseySettings.Value;
        }

        public async Task<string> GetCaseDetailsByCaseNumber(string caseNumber, int retry, int sleep, int timeout)
        {
            if (retry <= 0)
                throw new ArgumentOutOfRangeException(nameof(retry));

            if (String.IsNullOrEmpty(caseNumber))
                throw new ArgumentNullException(caseNumber);

            var request = new RestRequest();
            var body = IxmlMessages.BuildCaseByCaseNumber(_odysseySiteId, caseNumber);
            request.AddParameter(ServiceConstants.TextXml, body, ParameterType.RequestBody);

            // The default request timeout is 100s, overwirte it base on need
            request.Timeout = timeout;
            IRestResponse response = new RestResponse();

            while (true)
            {
                try
                {
                    response = await _restClientIxml.ExecutePostTaskAsync(request);
                    break; // success!
                }
                catch
                {
                    if (--retry == 0)
                        throw;
                    Thread.Sleep(sleep);
                }
            }

            var parsed = WebUtility.HtmlDecode(response.Content);
            
            return parsed;
        }

        public async Task<bool> AddDispositionEvent(string caseId, int retry, int sleep, int timeout)
        {
            var charges = GetCharges(caseId, retry, sleep, timeout);
            if (charges != null)
            {
                var chargesFound = charges.ToDictionary(item => item.ChargeId, item => ServiceConstants.DispositionCharge);
                var addDispoEvent = new AddDispositionEvent(int.Parse(_odysseySettings.TrafficNode), ServiceConstants.Reference, int.Parse(_odysseySettings.OdysseyDmvUser),
                    ServiceConstants.Source, caseId, DateTime.Now.ToShortDateString(), ServiceConstants.MessageDisposeViaCollection, chargesFound);
                var body = addDispoEvent.ToXml();

                var request = new RestRequest();
                request.Timeout = timeout;
                request.AddParameter(ServiceConstants.TextXml, body, ParameterType.RequestBody);

                IRestResponse response = new RestResponse();

                while (true)
                {
                    try
                    {
                        response = await _restClientApi.ExecutePostTaskAsync(request);
                        break; // success!
                    }
                    catch
                    {
                        if (--retry == 0)
                            throw;
                        Thread.Sleep(sleep);
                    }
                }

                var parsed = WebUtility.HtmlDecode(response.Content);

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new ApiException(ServiceConstants.MessageFailToAddDisposition + " Response Status Code: " + response.StatusCode);
                }

                try
                {
                    CourtFineDispoChargesResponse.Create(parsed);
                    return true;
                }
                catch (ApiException ex)
                {
                    if (ex.Message == "Already has a disposition")
                    {
                        return true;
                    }

                    throw;

                }
            }

            return false;
        }

        public List<Charges> GetCharges(string caseId, int retry, int sleep, int timeout)
        {
            var body = IxmlMessages.BuildChargesByCaseId(_odysseySiteId, caseId);

            var request = new RestRequest(Method.POST);
            request.Timeout = timeout; 
            request.AddHeader(ServiceConstants.ContentType, ServiceConstants.TextXml);
            request.AddParameter(ServiceConstants.TextXml, body, ParameterType.RequestBody);

            IRestResponse response = new RestResponse();

            while (true)
            {
                try
                {
                    response = _restClientIxml.Execute(request);
                    break; // success!
                }
                catch
                {
                    if (--retry == 0)
                        throw;
                    Thread.Sleep(sleep);
                }
            }

            if (response.StatusCode != HttpStatusCode.OK) return null;

            var parsed = WebUtility.HtmlDecode(response.Content);

            try
            {
                var charge = new Charges().Create(parsed);
                return charge;
            }
            catch (ApiException)
            {
                return null;
            }
        }
    }
}
