﻿using CriminalCollectionService.Core.Interfaces;
using CriminalCollectionService.Core.Models;
using CriminalCollectionService.Core.VOs;
using Microsoft.Extensions.Options;
using System;
using System.Threading.Tasks;

namespace CriminalCollectionService.Core.Services
{
    public class CollectionService : ICollectionService
    {
        private readonly IOdysseyService _odysseyService;
        private readonly ServiceSettingsVo _serviceSettings;
        public CollectionService(IOdysseyService odysseyService, IOptions<ServiceSettingsVo> serviceSettings)
        {
            _odysseyService = odysseyService;
            _serviceSettings = serviceSettings.Value;
        }

        public async Task<CollectionCase> GetCaseDetailsByCaseNumber(string caseNumber)
        {
            if (String.IsNullOrEmpty(caseNumber))
                throw new ArgumentNullException(caseNumber);

            // call odyssey service
            var response = await _odysseyService.GetCaseDetailsByCaseNumber(caseNumber, int.Parse(_serviceSettings.Retry),
                int.Parse(_serviceSettings.Sleep), int.Parse(_serviceSettings.Timeout));

            var integrationNode = CaseDetail.GetIntegrationNode(response);
            var caseDetail = CaseDetail.CreateCollectionCase(integrationNode);

            return caseDetail;
        }

        public async Task<bool> AddDispositionForCharges(int caseId)
        {
            if (caseId <= 0)
                throw new ArgumentNullException(caseId.ToString());

            // call odyssey service
            return await _odysseyService.AddDispositionEvent(caseId.ToString(), int.Parse(_serviceSettings.Retry), 
                int.Parse(_serviceSettings.Sleep), int.Parse(_serviceSettings.Timeout));
        }
    }
}
