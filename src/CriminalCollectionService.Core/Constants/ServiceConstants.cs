﻿namespace CriminalCollectionService.Core.Constants
{
    public static class ServiceConstants
    {
        // appsettings
        public const string ConnectionStrings = "ConnectionStrings";
        public const string OdysseySettings = "OdysseySettings";
        public const string ServiceSettings = "ServiceSettings";

        // http request
        public const string ContentType = "Content-Type";
        public const string TextXml = "text/xml";
        public const string requestType = "IXML";
        public const string requestName = "GetCaseDetailsByCaseNumber";

        // Odyssey
        public const string DispositionCharge = "FGC";
        public const string Reference = "0";
        public const string Source = "0";
        public const int UserId = 1;

        // Message
        public const string MessageResponseNot200 = "Http status code is NOT 200";
        public const string MessageRequestUnsuccess = "Request was unsuccessful. Retrying for case ";
        public const string MessageIxmlFailGetCaseData = "IXML Server failed to get case data";
        public const string MessageFailToAddDisposition = "Odyssey Transaction failed to add disposition event";
        public const string MessageDisposeViaCollection = "Disposed via Collection integration program";
        public const string MessageSuccessDisposition = "Success add disposition for CaseID ";
    }
}
