﻿using static System.String;

namespace CriminalCollectionService.Core.Constants
{
    public static class CoreConstants
    {
        // Url
        public const string SoapUrl = "http://schemas.xmlsoap.org/soap/envelope/";
        public const string TylerUrl = "http://www.tylertech.com";
        public const string TylerUrlSlash = "http://www.tylertech.com/";

        // Soap response and result
        public const string LoadIntegrationXmlResponse = "LoadIntegrationXmlResponse";
        public const string LoadIntegrationXmlResult = "LoadIntegrationXmlResult";
        public const string OdysseyMsgExecutionResponse = "OdysseyMsgExecutionResponse";
        public const string OdysseyMsgExecutionResult = "OdysseyMsgExecutionResult";
        public const string ErrorStream = "ERRORSTREAM";
        public const string Integration = "Integration";
        public const string Body = "Body";
        public const string Result = "Result";
        public const string DISPLAYMESSAGE = "DISPLAYMESSAGE";

        // Case Disposition
        public const string AddDispositionEvent = "AddDispositionEvent";
        public const string DispositionEventId = "DispositionEventID";

        //Message
        public const string MessageSoapError = "IXML response has error";
        public const string MessageNoIntegrationNode = "IXML response has no Integration node";
        public const string MessageNoCaseNode = "IXML response has no Case node";
        public const string MessageFailGetChargeData = "IXML Server failed to get charge data";
        public const string MessageNoCaseFound = "No Case Found";
        public const string MessageSuccess = "Successful";
        public const string MessageIxmlResponseUnSuccessful = "IXML response code is NOT 200";
        public const string MessageNoFeeInstance = "No fee instance";
        public const string MessageNoFeeData = "No fee data";
        public const string MessageNoFeeCategoryBalance = "No fee category balance";
        public const string MessageFailDisposition = "Failed to add disposition event";
        public const string MessageAlreadyHasDisposition = "Already has a disposition";
        public const string MessageTrafficCaseFound = "Found Traffic Case {0}";
        public const string MessageCaseNumberNotFound = "Case number {0} was not found";
        public const string MessageMissingValuesFor = "Required values are missing for {0}";

        // Exception
        public const string NoCasesFoundException = "Tyler.Odyssey.API.Case.NoCasesFoundException";

        // Date
        public const string DateFormat = "MM/dd/yyyy";

        // Case general info
        public const string Case = "Case";
        public const string CaseNumber = "CaseNumber";
        public const string CaseType = "CaseType";
        public const string CaseCrossReference = "CaseCrossReference";
        public const string CrossCaseNumber = "CrossCaseNumber";
        public const string Court = "Court";
        public const string CourtName = "CourtName";
        public const string Word = "Word";
        public const string True = "true";

        // Attorney
        public const string ReportingAgency = "ReportingAgency";
        public const string Agency = "Agency";
        public const string Attorney = "Attorney";

        // Case party
        public const string Id = "ID";
        public const string Party = "Party";
        public const string OtherId = "OtherID";
        public const string OtherIdNumber = "OtherIDNumber";
        public const string OtherIdAgency = "OtherIDAgency";
        public const string Pfn = "PFN";

        public const string CaseParty = "CaseParty";
        public const string CasePartyName = "CasePartyName";
        public const string CasePartyAttorney = "CasePartyAttorney";
        public const string CaseLink = "CaseLink";
        public const string Connection = "Connection";
        public const string Description = "Description";
        public const string Defendant = "Defendant";

        public const string PartyName = "PartyName";
        public const string NameType = "NameType";
        public const string Gender = "Gender";
        public const string NameFirst = "NameFirst";
        public const string NameLast = "NameLast";
        public const string NameMiddle = "NameMiddle";

        public const int PhoneMaxOccurs = 4;
        public const string Phone = "Phone";
        public const string Current = "Current";
        public const string Number = "Number";
        public const string Extension = "Extension";
        public const string Type = "Type";

        public const string SocialSecurityNumber = "SocialSecurityNumber";
        public const string DateOfBirth = "DateOfBirth";
        public const string DriversLicense = "DriversLicense";
        public const string DriversLicenseState = "DriversLicenseState";
        public const string DriversLicenseNumber = "DriversLicenseNumber";

        public const string PartyCurrent = "PartyCurrent";
        public const string Address = "Address";
        public const string AddressLine1 = "AddressLine1";
        public const string AddressLine2 = "AddressLine2";
        public const string AddressLine3 = "AddressLine3";
        public const string AddressLine4 = "AddressLine4";
        public const string City = "City";
        public const string State = "State";
        public const string Zip = "Zip";

        // Charge
        public const int ChargeMaxOccurs = 15;
        public const string Charge = "Charge";
        public const string ChargeOffenseDate = "ChargeOffenseDate";
        public const string ChargeHistory = "ChargeHistory";
        public const string Statute = "Statute";
        public const string StatuteNumber = "StatuteNumber";
        public const string StatuteCode = "StatuteCode";
        public const string Degree = "Degree";

        public const string InternalChargeId = "InternalChargeID";
        public const string CurrentCharge = "CurrentCharge";
        public const string GeneralOffenseCode = "GeneralOffenseCode";
        public const string Tse = "TSE";

        // Fee
        public const int FeeMaxOccurs = 60;
        public const string Fees = "Fees";
        public const string FeeCategoryBalances = "FeeCategoryBalances";
        public const string PartyBalance = "PartyBalance";
        public const string Balance = "Balance";
        public const string FeeInstances = "FeeInstances";
        public const string FeeInstance = "FeeInstance";
        public const string FeeCode = "FeeCode";
        public const string ChargeAmount = "ChargeAmount";
        public const string PaymentAmount = "PaymentAmount";

        // Helper
        public static class Helpers
        {
            public static string TrafficCaseFound(string caseNumber)
            {
                return Format(MessageTrafficCaseFound, caseNumber);
            }

            public static string CaseNumberNotFound(string caseNumber)
            {
                return Format(MessageCaseNumberNotFound, caseNumber);
            }

            public static string MissingValuesFor(string method)
            {
                return Format(MessageMissingValuesFor, method);
            }

        }
    }
}
