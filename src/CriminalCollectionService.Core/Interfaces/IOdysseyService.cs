﻿using CriminalCollectionService.Core.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CriminalCollectionService.Core.Interfaces
{
    public interface IOdysseyService
    {
        Task<string> GetCaseDetailsByCaseNumber(string caseNumber, int retry, int sleep, int timeout);

        Task<bool> AddDispositionEvent(string caseId, int retry, int sleep, int timeout);

        List<Charges> GetCharges(string caseId, int retry, int sleep, int timeout);
    }
}
