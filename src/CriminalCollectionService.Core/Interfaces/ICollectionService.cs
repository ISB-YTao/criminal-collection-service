﻿using System.Threading.Tasks;

namespace CriminalCollectionService.Core.Interfaces
{
    public interface ICollectionService
    {
        Task<CollectionCase> GetCaseDetailsByCaseNumber(string caseNumber);
        Task<bool> AddDispositionForCharges(int caseId);
    }
}
