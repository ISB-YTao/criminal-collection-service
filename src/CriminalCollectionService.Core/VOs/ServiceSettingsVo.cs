﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CriminalCollectionService.Core.VOs
{
    public class ServiceSettingsVo
    {
        public string Retry { get; set; }
        public string Sleep { get; set; }
        public string Timeout { get; set; }
    }
}
