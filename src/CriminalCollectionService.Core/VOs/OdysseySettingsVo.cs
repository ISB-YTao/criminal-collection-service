﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CriminalCollectionService.Core.VOs
{
    public class OdysseySettingsVo
    {
        public string OdysseyApiServer { get; set; }
        public string OdysseyApiEndpoint { get; set; }
        public string OdysseyIxmlServer { get; set; }
        public string OdysseyIxmlEndpoint { get; set; }
        public string OdysseySiteKey { get; set; }
        public string OdysseyDmvUser { get; set; }
        public string TrafficNode { get; set; }
    }
}
