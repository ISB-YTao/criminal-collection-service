﻿namespace CriminalCollectionService.Core.Models
{
    public abstract class MapperBase
    {
        public bool IsValidMap { get; private set; }

        public void SetAsValid()
        {
            IsValidMap = true;
        }

        public void SetAsInvalid()
        {
            IsValidMap = false;
        }
    }
   
}