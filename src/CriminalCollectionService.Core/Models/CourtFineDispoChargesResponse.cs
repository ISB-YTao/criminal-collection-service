﻿using CriminalCollectionService.Core.Constants;
using CriminalCollectionService.Core.Exceptions;
using System.Linq;
using System.Xml.Linq;

namespace CriminalCollectionService.Core.Models
{
    public class CourtFineDispoChargesResponse : MapperBase
    {
        public string DispositionEventId { get; set; }
        public string StatisticalClosureId { get; set; }

        public CourtFineDispoChargesResponse(){}

        public static CourtFineDispoChargesResponse Create(string xml)
        {
                var mapperObject = new CourtFineDispoChargesResponse();
                XDocument xdoc = XDocument.Parse(xml);
                XNamespace ns = CoreConstants.SoapUrl;
                XNamespace def = CoreConstants.TylerUrlSlash;
                var soapBody = xdoc.Descendants(ns + CoreConstants.Body).FirstOrDefault();
                var soapRes = soapBody.Descendants(def + CoreConstants.OdysseyMsgExecutionResponse).FirstOrDefault();
                var soapResult = soapRes.Descendants(def + CoreConstants.OdysseyMsgExecutionResult).FirstOrDefault();

                var soapError = soapResult.Descendants(def + CoreConstants.ErrorStream).FirstOrDefault();
                if (soapError != null)
                {
                    var errorMessage = soapError.Descendants(def + CoreConstants.DISPLAYMESSAGE).FirstOrDefault().Value;
                    if (errorMessage.Contains("already has a disposition"))
                    {
                        throw new ApiException(CoreConstants.MessageAlreadyHasDisposition);
                    }
                    else
                    {
                        throw new ApiException(CoreConstants.MessageFailDisposition);
                    }
                }

                var dispoElement = soapResult.Descendants(def + CoreConstants.Result).FirstOrDefault();
                if (dispoElement != null)
                {
                    mapperObject.DispositionEventId = (string)dispoElement.Element(def + CoreConstants.DispositionEventId) ?? string.Empty;
                }

                if (string.IsNullOrEmpty(mapperObject.DispositionEventId))
                {
                    mapperObject.SetAsInvalid();
                }
                else
                {
                    mapperObject.SetAsValid();
                }

                return mapperObject;
        }
    }
}