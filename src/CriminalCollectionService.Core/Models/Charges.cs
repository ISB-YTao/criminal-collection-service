﻿using CriminalCollectionService.Core.Constants;
using CriminalCollectionService.Core.Exceptions;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace CriminalCollectionService.Core.Models
{
    /// <summary>
    /// This class is used to generate a list of charges for determining dispositions
    /// </summary>
    public class Charges
    {
        public string ChargeId { get; set; }
        public bool IsTse { get; set; } = false;

        public Charges(){ }
        public Charges(string chargeId, bool isTse)
        {
            ChargeId = chargeId;
            IsTse = isTse;
        }

        public List<Charges> Create(string xml)
        {
            List<Charges> listOfCharges = new List<Charges>();
            XDocument xdoc = XDocument.Parse(xml);
            XNamespace ns = CoreConstants.SoapUrl;
            XNamespace def = CoreConstants.TylerUrl;
            var soapBody = xdoc.Descendants(ns + CoreConstants.Body).FirstOrDefault();
            var soapRes = soapBody.Descendants(def + CoreConstants.LoadIntegrationXmlResponse).FirstOrDefault();
            var soapResult = soapRes.Descendants(def + CoreConstants.LoadIntegrationXmlResult).FirstOrDefault();
            var soapIntegration = soapResult.Descendants(def + CoreConstants.Integration).FirstOrDefault();

            var soapError = soapResult.Descendants(def + CoreConstants.ErrorStream).FirstOrDefault();
            if (soapError != null) throw new ApiException(CoreConstants.MessageFailGetChargeData);
          
            var payload = soapIntegration.Descendants(def + CoreConstants.Case).FirstOrDefault();

            var charges = payload.Descendants(def + CoreConstants.Charge);
            foreach (var charge in charges)
            {      
                var addTo = new Charges();
                addTo.ChargeId = (string)charge.Attribute(CoreConstants.InternalChargeId) ?? string.Empty;

                var chargeHistorys = charge.Descendants(def + CoreConstants.ChargeHistory);
                foreach(var chargeHistory in chargeHistorys)
                {
                    if(chargeHistory.Attribute(CoreConstants.CurrentCharge) != null && chargeHistory.Attribute(CoreConstants.CurrentCharge).Value == CoreConstants.True.ToLower())
                    {
                        if(chargeHistory.Element(def + CoreConstants.GeneralOffenseCode) != null)
                        {
                            var checkTse = chargeHistory.Element(def + CoreConstants.GeneralOffenseCode);
                            var eqTse = checkTse.Attribute(CoreConstants.Word)?.Value;
                            if (eqTse == CoreConstants.Tse) addTo.IsTse = true;
                            listOfCharges.Add(addTo);
                        }
                        else
                        {
                            listOfCharges.Add(addTo);
                        }
                    }

                }        
            }
            return listOfCharges;
        }

    }
}