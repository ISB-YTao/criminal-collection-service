﻿using System;
using System.Linq;
using System.Xml.Linq;
using CriminalCollectionService.Core.Constants;
using CriminalCollectionService.Core.Exceptions;

namespace CriminalCollectionService.Core.Models
{
    public class CaseDetail
    {
        private static readonly XNamespace def = CoreConstants.TylerUrl;

        public static XElement GetIntegrationNode(string xml)
        {
            XDocument xdoc = XDocument.Parse(xml);
            XNamespace ns = CoreConstants.SoapUrl;
            var soapBody = xdoc.Descendants(ns + CoreConstants.Body).FirstOrDefault();
            var soapRes = soapBody.Descendants(def + CoreConstants.LoadIntegrationXmlResponse).FirstOrDefault();
            var soapResult = soapRes.Descendants(def + CoreConstants.LoadIntegrationXmlResult).FirstOrDefault();

            var soapError = soapResult.Descendants(def + CoreConstants.ErrorStream).FirstOrDefault();
            if (soapError != null)
            {
                if (soapError.ToString().Contains(CoreConstants.NoCasesFoundException))
                {
                    throw new ApiException(CoreConstants.MessageNoCaseFound);
                }

                throw new ApiException(CoreConstants.MessageSoapError);
            }

            var collectionCase = new CollectionCase();

            if (soapResult.Element(def + CoreConstants.Integration) != null)
            {
                return soapResult.Element(def + CoreConstants.Integration);
            }
            else
            {
                throw new ApiException(CoreConstants.MessageNoIntegrationNode);
            }

        }

        public static string GetCaseId(XElement soapIntegration)
        {
            string caseId = String.Empty;
            if (soapIntegration.Element(def + CoreConstants.Case) != null)
            {
                var soapCase = soapIntegration.Descendants(def + CoreConstants.Case).FirstOrDefault();
                caseId = soapCase.Attribute("InternalID").Value ?? String.Empty;               
            }

            return caseId;
        }
        public static CollectionCase CreateCollectionCase(XElement soapIntegration)
        {

            var collectionCase = new CollectionCase();
            if (soapIntegration.Element(def + CoreConstants.Case) != null)
            {
                var soapCase = soapIntegration.Descendants(def + CoreConstants.Case).FirstOrDefault();
                // get case data
                collectionCase = GetCaseData(collectionCase, soapCase);

                // get party data
                collectionCase = GetPartyData(collectionCase, soapIntegration, soapCase);

                // get charge data
                collectionCase = GetChargeData(collectionCase, soapCase);

                // get fee data
                collectionCase = GetFeeData(collectionCase, soapIntegration);
            }
            else
            {
                throw new ApiException(CoreConstants.MessageNoCaseNode);
            }

            return collectionCase;
        }

        private static CollectionCase GetCaseData(CollectionCase caseDetail, XElement soapCase)
        {
            caseDetail.OADOCKET = (string)soapCase.Element(def + CoreConstants.CaseNumber) ?? String.Empty;
            caseDetail.OACASETYPE = (string)soapCase.Element(def + CoreConstants.CaseType) ?? String.Empty;
            caseDetail.OACOURT = (string)soapCase.Descendants(def + CoreConstants.Court).FirstOrDefault()
                                                 .Element(def + CoreConstants.CourtName) ?? String.Empty;

            if (soapCase.Element(def + CoreConstants.Attorney) != null)
            {
                caseDetail.OADEFATTY = new CollectionCaseOADEFATTY();
                var name = soapCase.Descendants(def + CoreConstants.Attorney).FirstOrDefault()
                                   .Descendants(def + CoreConstants.CasePartyName).FirstOrDefault();

                if (name.Element(def + CoreConstants.NameFirst) != null)
                {
                    caseDetail.OADEFATTY.OAATTYFNAM = (string)name.Element(def + CoreConstants.NameFirst) ?? String.Empty;
                }

                if (name.Element(def + CoreConstants.NameLast) != null)
                {
                    caseDetail.OADEFATTY.OAATTYLNAM = (string)name.Element(def + CoreConstants.NameLast) ?? String.Empty;
                }

                if (name.Element(def + CoreConstants.NameMiddle) != null)
                {
                    caseDetail.OADEFATTY.OAATTYMNAM = (string)name.Element(def + CoreConstants.NameMiddle) ?? String.Empty;
                }
            }

            return caseDetail;
        }

        private static CollectionCase GetChargeData(CollectionCase caseDetail, XElement soapCase)
        {
            int i = 0;
            caseDetail.OACHRG = new CollectionCaseOACHRG[CoreConstants.ChargeMaxOccurs];

            foreach (var charge in soapCase.Descendants(def + CoreConstants.Charge))
            {
                if (String.IsNullOrEmpty(caseDetail.OAVIOLDATE))
                {
                    caseDetail.OAVIOLDATE = (string)charge.Element(def + CoreConstants.ChargeOffenseDate) ?? String.Empty;
                }
                
                if (charge.Element(def + CoreConstants.ReportingAgency) != null)
                {
                    caseDetail.OAFILEAGENCY = (string)charge.Descendants(def + CoreConstants.ReportingAgency).FirstOrDefault()
                                                            .Element(def + CoreConstants.Agency)
                                                            .Attribute(CoreConstants.Word).Value ?? String.Empty;
                }

                var chargeHistory = charge.Descendants(def + CoreConstants.ChargeHistory).FirstOrDefault();
                if (chargeHistory != null && chargeHistory.Element(def + CoreConstants.Statute) != null)
                {
                    var statute = chargeHistory.Element(def + CoreConstants.Statute);
                    CollectionCaseOACHRG chargeStatute = new CollectionCaseOACHRG();

                    chargeStatute.OASTATCODE = (string) statute.Element(def + CoreConstants.StatuteNumber) ?? String.Empty;
                    if (statute.Element(def + CoreConstants.StatuteCode) != null)
                    {
                        chargeStatute.OASECTCODE =
                            (string) statute.Element(def + CoreConstants.StatuteCode).Attribute(CoreConstants.Word) ??
                            String.Empty;

                        // need to remove first two letter if it already contains StatuteNumber. e.g. "VC"
                        if (chargeStatute.OASECTCODE.Substring(0, 2).Equals(chargeStatute.OASTATCODE))
                        {
                            chargeStatute.OASECTCODE = chargeStatute.OASECTCODE.Substring(2);
                        }
                    }

                    if (statute.Element(def + CoreConstants.Degree) != null)
                    {
                        chargeStatute.OAOFNSLVL =
                            (string) statute.Element(def + CoreConstants.Degree).Attribute(CoreConstants.Word) ?? String.Empty;
                    }

                    if (i < CoreConstants.ChargeMaxOccurs)
                    {
                        caseDetail.OACHRG[i++] = chargeStatute;
                    }
                    else
                    {
                        break;
                    }
                }
            }

            return caseDetail;
        }

        private static CollectionCase GetFeeData(CollectionCase caseDetail, XElement soapIntegration)
        {
            if (soapIntegration.Element(def + CoreConstants.Fees) != null)
            {
                var soapFee = soapIntegration.Descendants(def + CoreConstants.Fees).FirstOrDefault();
                if (soapFee.Element(def + CoreConstants.FeeCategoryBalances) != null)
                {
                    caseDetail.OAFINEAMOUNT =
                        (decimal) soapFee.Descendants(def + CoreConstants.FeeCategoryBalances).FirstOrDefault()
                        .Descendants(def + CoreConstants.PartyBalance).FirstOrDefault()
                        .Element(def + CoreConstants.Balance);
                }
                else
                {
                    throw new ApiException(CoreConstants.MessageNoFeeCategoryBalance);
                }

                if (soapFee.Element(def + CoreConstants.FeeInstances) != null)
                {
                    var feeInstances = soapFee.Descendants(def + CoreConstants.FeeInstances).FirstOrDefault();

                    int k = 0;
                    caseDetail.OAFEE = new CollectionCaseOAFEE[CoreConstants.FeeMaxOccurs];

                    foreach (var feeInstance in feeInstances.Descendants(def + CoreConstants.FeeInstance))
                    {
                        CollectionCaseOAFEE fee = new CollectionCaseOAFEE();
                        fee.OAFEECODE = (string) feeInstance.Element(def + CoreConstants.FeeCode).Attribute(CoreConstants.Word) ?? String.Empty;
                        fee.OAFEEAMT = (decimal) feeInstance.Element(def + CoreConstants.ChargeAmount) - (decimal)feeInstance.Element(def + CoreConstants.PaymentAmount);
                        if (k < CoreConstants.FeeMaxOccurs)
                        {
                            caseDetail.OAFEE[k++] = fee;
                        }
                        else
                        {
                            break;
                        }

                    }
                }
                else
                {
                    throw new ApiException(CoreConstants.MessageNoFeeInstance);
                }
            }
            else
            {
                throw new ApiException(CoreConstants.MessageNoFeeData);
            }
            
            return caseDetail;
        }

        private static CollectionCase GetPartyData(CollectionCase caseDetail, XElement soapIntegration, XElement soapCase)
        {
            string partyId = String.Empty;

            foreach (var caseParty in soapCase.Descendants(def + CoreConstants.CaseParty))
            {
                var description = (string)caseParty.Descendants(def + CoreConstants.Connection).FirstOrDefault()
                                                   .Element(def + CoreConstants.Description) ?? String.Empty;
                
                if (description.Contains(CoreConstants.Defendant))
                {
                    partyId = caseParty.Attribute(CoreConstants.Id).Value ?? String.Empty;
                    if (caseParty.Element(def + CoreConstants.CasePartyName) != null)
                    {
                        // Full name
                        caseDetail.OAFNAM = (string)caseParty.Descendants(def + CoreConstants.CasePartyName).FirstOrDefault()
                                                .Element(def + CoreConstants.NameFirst) ?? String.Empty;
                        caseDetail.OALNAM = (string)caseParty.Descendants(def + CoreConstants.CasePartyName).FirstOrDefault()
                                                .Element(def + CoreConstants.NameLast) ?? String.Empty;
                        caseDetail.OAMNAM = (string)caseParty.Descendants(def + CoreConstants.CasePartyName).FirstOrDefault()
                                                .Element(def + CoreConstants.NameMiddle) ?? String.Empty;
                    }
                    break;
                }
            }

            foreach (var party in soapIntegration.Descendants(def + CoreConstants.Party))
            {
                if (party.Attribute(CoreConstants.Id).Value == partyId)
                {
                    // PFN
                    foreach (var otherId in party.Descendants(def + CoreConstants.OtherId))
                    {
                        if ((string) otherId.Element(def + CoreConstants.OtherIdAgency) == CoreConstants.Pfn)
                        {
                            caseDetail.OAPFN = (string) otherId.Element(def + CoreConstants.OtherIdNumber) ??
                                               String.Empty;
                        }
                    }

                    // Gender
                    if (party.Element(def + CoreConstants.Gender) != null)
                    {
                        caseDetail.OAGEN =
                            (string) party.Element(def + CoreConstants.Gender).Attribute(CoreConstants.Word).Value ??
                            String.Empty;
                    }

                    // SSN, DOB
                    caseDetail.OASSN = (string) party.Element(def + CoreConstants.SocialSecurityNumber) ?? String.Empty;
                    caseDetail.OADOB = (string) party.Element(def + CoreConstants.DateOfBirth) ?? String.Empty;

                    // Driver's License
                    foreach (var driversLicense in party.Descendants(def + CoreConstants.DriversLicense))
                    {
                        if (driversLicense.Attribute(CoreConstants.Current) != null)
                        {
                            if (driversLicense.Attribute(CoreConstants.Current).Value == CoreConstants.True)
                            {
                                caseDetail.OADLNNUMBER =
                                    (string) driversLicense.Element(def + CoreConstants.DriversLicenseNumber) ?? 
                                    String.Empty;
                                caseDetail.OADLNSTATE =
                                    (string)driversLicense.Element(def + CoreConstants.DriversLicenseState)
                                        .Attribute(CoreConstants.Word) ?? String.Empty;
                                break;
                            }
                        }
                    }

                // Phone Numbers
                    int j = 0;
                    caseDetail.OAPHONE = new CollectionCaseOAPHONE[CoreConstants.PhoneMaxOccurs];

                    foreach (var phone in party.Descendants(def + CoreConstants.Phone))
                    {
                        if (phone != null && phone.Attribute(CoreConstants.Current) != null)
                        {
                            if (phone.Attribute(CoreConstants.Current).Value == CoreConstants.True)
                            {
                                CollectionCaseOAPHONE contact = new CollectionCaseOAPHONE();
                                contact.TYPE = (string)phone.Element(def + CoreConstants.Type) ?? String.Empty;
                                contact.NUMBER = (string)phone.Element(def + CoreConstants.Number) ?? String.Empty;

                                if (phone.Element(def + CoreConstants.Extension) != null)
                                {
                                    contact.EXTENSIONSpecified = true;
                                    contact.EXTENSION = (ushort)(int)phone.Element(def + CoreConstants.Extension);
                                }

                                if (j < CoreConstants.PhoneMaxOccurs)
                                {
                                    caseDetail.OAPHONE[j++] = contact;
                                }
                                else
                                {
                                    break;
                                }
                            }
                            
                        }
                    }

                    // Address
                    if (party.Element(def + CoreConstants.Address) != null)
                    {

                        foreach (var address in party.Descendants(def + CoreConstants.Address))
                        {
                            try
                            {
                                if (address.Attribute(CoreConstants.PartyCurrent).Value == CoreConstants.True)
                                {
                                    CollectionCaseOAADDR addr = new CollectionCaseOAADDR();
                                    addr.ADDRESS1 = (string)address.Element(def + CoreConstants.AddressLine1) ?? String.Empty;
                                    addr.ADDRESS2 = (string)address.Element(def + CoreConstants.AddressLine2) ?? String.Empty;
                                    addr.ADDRESS3 = (string)address.Element(def + CoreConstants.AddressLine3) ?? String.Empty;
                                    addr.ADDRESS4 = (string)address.Element(def + CoreConstants.AddressLine4) ?? String.Empty;
                                    addr.CITY = (string)address.Element(def + CoreConstants.City) ?? String.Empty;
                                    addr.STATE = (string)address.Element(def + CoreConstants.State) ?? String.Empty;
                                    addr.ZIP = (string)address.Element(def + CoreConstants.Zip) ?? String.Empty;

                                    caseDetail.OAADDR = addr;
                                    break;
                                }
                            }
                            catch (Exception)
                            {
                                // skip if no partycurrent attribute available
                            }

                        }
                    }
                }
            }

            return caseDetail;
        }
    }
}